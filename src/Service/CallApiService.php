<?php

namespace App\Service;

use DateTime;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class CallApiService
{
    private $client;

    public function __construct(HttpClientInterface $client)
    {
        $this->client = $client;
    }

    public function getFranceData(): array
    {
        return $this->getApi('live/france');
    }

    public function getAllData(): array
    {
        return $this->getApi('live/departements');
    }

    public function getDepartementData($departement): array
    {
        $data = $this->getApi('live/france');
        $date = $data[0]['date'];
        $date = new DateTime($date);
        $date = date_format($date, 'd-m-Y');

        $departement = strtolower($departement);
    
        $search  = array('À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'Ù', 'Ú', 'Û', 'Ü', 'Ý', 'à', 'á', 'â', 'ã', 'ä', 'å', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ð', 'ò', 'ó', 'ô', 'õ', 'ö', 'ù', 'ú', 'û', 'ü', 'ý', 'ÿ');
        $replace = array('A', 'A', 'A', 'A', 'A', 'A', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'O', 'O', 'O', 'O', 'O', 'U', 'U', 'U', 'U', 'Y', 'a', 'a', 'a', 'a', 'a', 'a', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'o', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u', 'y', 'y');
        $depFormat = str_replace($search, $replace, $departement);
    
    /*
        setlocale(LC_CTYPE, 'fr_FR');
        $depSansAccent = iconv('UTF-8', 'ASCII//TRANSLIT//IGNORE', $departement);
    */
        return $this->getApi('departement/' . $depFormat . '/' . $date  );
    }

    public function getAllDataByDate($date): array
    {
        return $this->getApi('departements-by-date/' . $date);
    }

    private function getApi(string $var)
    {
        $response = $this->client->request(
            'GET',
            'https://coronavirusapifr.herokuapp.com/data/' . $var
        );

        return $response->toArray();
    }
}