<?php

namespace App\Controller;

use App\Service\CallApiService;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\UX\Chartjs\Builder\ChartBuilderInterface;
use Symfony\UX\Chartjs\Model\Chart;

class DepartementController extends AbstractController
{
    /**
     * @Route("/departement/{departement}", name="departement")
     */
    public function index(string $departement, CallApiService $callApiService, ChartBuilderInterface $chartBuilder): Response
    {
        $label = [];
        $hospitalisation =  [];
        $rea = [];

        for ($i=1; $i < 8; $i++)
        {
            $date = new DateTime('- ' . $i .' day');
            $datas = $callApiService->getAllDataByDate($date->format('d-m-Y'));

            foreach ($datas as $data){
                if( $data['lib_dep'] === $departement) {
                    $label[] = $data['date'];
                    $hospitalisation[] = $data['incid_hosp'];
                    $rea[] = $data['incid_rea'];
                    break;
                }
            }
        }

        $chart = $chartBuilder->createChart(Chart::TYPE_LINE);
        $chart->setData([
            'labels' => array_reverse($label),
            'datasets' => [
                [
                    'label' => "Nouvelles Hospitalisations",
                    'borderColor' => 'rgb(255, 99, 132)',
                    'data' => array_reverse($hospitalisation),
                    'tension' => 0.1,
                    'fill' => false,
                ],  
                [
                    'label' => "Nouvelles entrées en Réa",
                    'borderColor' => 'rgb(46, 41, 78)',
                    'data' => array_reverse($rea),
                    'tension' => 0.1,
                    'fill' => false,
                ], 
            ],
        ]);

        $chart->setOptions([/* ... */]);

        return $this->render('departement/index.html.twig', [
            'data' => $callApiService->getDepartementData($departement),
            'chart' => $chart,
        ]);
    }
}
